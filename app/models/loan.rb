class Loan < ActiveRecord::Base
  belongs_to :quote


  #validation
  validates :down_payment ,presence: true
  validates :interest_rate ,presence: true


end
