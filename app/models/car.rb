class Car < ActiveRecord::Base
  has_many :quotes

  before_save :cal_markup

  validates :model ,presence: true
  validates :color ,presence: true
  validates :vin ,presence: true
  validates :price ,presence: true



  def self.search(search)
    if search
      where('model LIKE ? OR color LIKE ? OR vin LIKE ?', "%#{search}%", "%#{search}%", "%#{search}%")
    else
      scoped
    end
  end

  def car_details
    "#{model}, #{color}"
  end

  def update_status
    if self.car_status == true
      self.car_status=:sold

    else
      self.car_status=:unsold

    end
  end


  def cal_markup
    markup = (self.price * 1.1).round(2)
  end



end
