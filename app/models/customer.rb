class Customer < ActiveRecord::Base
  has_many :quotes

  validates :first_name ,presence: true
  validates :last_name ,presence: true
  validates :address ,presence: true
  validates :phone ,presence: true





  def self.search(search)
    if search
      where("first_name LIKE ? OR last_name LIKE ?", "%#{search}%", "%#{search}%")
    else
      scoped
    end

  end

  end