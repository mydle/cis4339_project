class Quote < ActiveRecord::Base
  belongs_to :car
  belongs_to :customer
  belongs_to :salesperson
  has_one :loan

  def quote_info
    "#{customer.first_name}, $#{final_price}"
  end



  def update_status
    if self.quote_status == true
      car.update_attribute(:car_status,true)
      self.quote_status=:sold

    else
      car.update_attribute(:car_status,false)
      self.quote_status=:unsold

    end
  end

  def final_pri
    self.final_price = (car.cal_markup * 1.043).round(2)
  end

end
