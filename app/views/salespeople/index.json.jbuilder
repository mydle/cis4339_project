json.array!(@salespeople) do |salesperson|
  json.extract! salesperson, :id, :name, :last_name, :phone
  json.url salesperson_url(salesperson, format: :json)
end
