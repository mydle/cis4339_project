json.array!(@quotes) do |quote|
  json.extract! quote, :id, :salesperson_id, :customer_id, :car_id, :car_price, :sale_tax, :final_price, :quote_status
  json.url quote_url(quote, format: :json)
end
