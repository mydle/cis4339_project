json.array!(@loans) do |loan|
  json.extract! loan, :id, :quote_id, :down_payment, :loan_amount, :loan_term, :interest_rate, :monthly_payment
  json.url loan_url(loan, format: :json)
end
