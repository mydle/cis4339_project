# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/

updateLoan1 = ->
  quoteid = $('#loan_quote_id').val()
  $.getJSON '/quotes/' + quoteid + '/quote_price', {},(json, response) ->

    quoteprice = json['final_price']

    downpayment = $('#loan_down_payment').val()

    borrow = quoteprice-downpayment

    $('#loan_loan_amount').val(borrow)

$ ->
  $('#loan_down_payment').bind 'change keyup mousewheel', -> updateLoan1()


# Calculation monthly payment after get borrow amount

updateLoan2 = ->

  amount_borrow = $('#loan_loan_amount').val()

  rate = $('#loan_interest_rate').val()

  loan_term = $('#loan_loan_term').val()

  r = rate / 1200

  n = r * amount_borrow
  d = 1 - (1 + r)**-((loan_term*12))
  payment = (n/d).toFixed(2)

  $('#loan_monthly_payment').val(payment)


$ ->
  $('#loan_interest_rate').bind 'change keyup mousewheel', -> updateLoan2()