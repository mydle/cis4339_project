# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/
updateMarkup = ->
  selection_id = $('#quote_car_id').val()
  $.getJSON '/cars/' + selection_id + '/markup', {},(json, response) ->
    msrp = json['price']
    markupprice= (msrp * 1.1).toFixed(2)
    $('#markup').html( "$" + markupprice + "USD")
    #
    SaleTax = (markupprice * 0.043).toFixed(2)
    $('#quote_sale_tax').val(SaleTax)
    Total = (parseFloat(markupprice) + parseFloat(SaleTax)).toFixed(2)
    $('#quote_final_price').val(Total)

$(document).on 'page:change', ->
  if $('#quote_car_id').length > 0
    updateMarkup()
  $('#quote_car_id').change -> updateMarkup()
