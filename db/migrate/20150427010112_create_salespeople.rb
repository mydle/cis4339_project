class CreateSalespeople < ActiveRecord::Migration
  def change
    create_table :salespeople do |t|
      t.string :name
      t.string :last_name
      t.string :phone

      t.timestamps null: false
    end
  end
end
