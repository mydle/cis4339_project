class CreateQuotes < ActiveRecord::Migration
  def change
    create_table :quotes do |t|
      t.integer :salesperson_id
      t.integer :customer_id
      t.integer :car_id
      t.decimal :car_price
      t.decimal :sale_tax
      t.decimal :final_price
      t.boolean :quote_status

      t.timestamps null: false
    end
  end
end
