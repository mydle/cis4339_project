class CreateLoans < ActiveRecord::Migration
  def change
    create_table :loans do |t|
      t.integer :quote_id
      t.decimal :down_payment
      t.decimal :loan_amount
      t.integer :loan_term
      t.decimal :interest_rate
      t.decimal :monthly_payment

      t.timestamps null: false
    end
  end
end
