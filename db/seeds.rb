# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Sale Person data
Salesperson.delete_all
salespeople = Salesperson.create ([  {name: 'Minh', last_name: 'Le', phone: '(713) 881-4336'},
                                     {name: 'Lucy', last_name: 'Nguyen', phone: '(832) 656-2434'},
                                     {name: 'Elly', last_name: 'Tran', phone: '(832) 687-9045'},
                                     {name: 'Tom', last_name: 'Lee', phone: '(713) 435-0784'},
                                     {name: 'Brad', last_name: 'Do', phone: '(832) 335-6446'}
                                 ])

# Customer data
Customer.delete_all
customers = Customer.create ([
                                {first_name: 'Tan', last_name: 'Ho', address: '8502 Sunny St. Houston, TX 77002', phone: '(713) 984-4756'},
                                {first_name: 'Mary', last_name: 'Nguyen', address: '5586 Allen Ave. Houston, TX 77003', phone: '(832) 484-2332'},
                                {first_name: 'Dillon', last_name: 'Le', address: '1976 Fannin St. Houston, TX 77389', phone: '(281) 574-9756'},
                                {first_name: 'Daniel', last_name: 'Nguyen', address: '6332 Dairy Ashford St. Houston, TX 77072', phone: '(713) 556-1215'},
                                {first_name: 'John', last_name: 'Dao', address: '8978 Main St. Sugarland, 77984', phone: '(713) 426-6990'},
                                {first_name: 'Emma', last_name: 'Lee', address: '5324 Bellaire Blvd. Houston, TX 77384', phone: '(832) 446-0967'},
                                {first_name: 'Tammy', last_name: 'Le', address: '5489 West Rd. Houston, TX 77989', phone: '(832) 240-2792'},
                                {first_name: 'Tyson', last_name: 'Do', address: '1869 Richmond Rd. Apt. 230 Houston, TX 77607', phone: '(281) 416-2657'},
                                {first_name: 'Collin', last_name: 'Nguyen', address: '9624 Smith St. Sugarland, 77989', phone: '(310) 980-5019'},
                                {first_name: 'Crystal', last_name: 'Liu', address: '2276 Dallas St. Apt. 171 Houston, TX 55732', phone: '(713) 320-7739'}

                            ])
# Car data
Car.delete_all
cars = Car.create ([
                      {model: 'Kia Chargino Flash-Fried XPath', color:'Red',vin: 'JH4TB2H26CC000000', price: '12000', },
                      {model: 'Toyota Gluino Roasted WATFOR', color:'White',vin: '1FMZU67E63U451701', price: '15000', },
                      {model: 'Volkswagen Majoron Boiled Ladder',color:'Blue',vin: 'WDDGF56X89F703120', price: '14000', },
                      {model: 'Cadillac Neutron Crispy S2',color:'Black',vin: 'WP0AA2A91AS204161', price: '40000', },
                      {model: 'BMW Dilaton Suckling Nice',color:'White',vin: '1D7GL36KX4S571303', price: '42000', },
                      {model: 'SUV Lexus RX 350', color: 'Silver Lining Metallic', vin:'1GTCS1942Y8539879', price: '35000', },
                      {model: 'SUV Lexus RX 350', color: 'Black', vin:'WAUHF78P67A613477', price: '32000', },
                      {model: 'SUV Cadillac RX 350', color: 'Deep Sea Mica', vin:'1D3HB13P89J511580', price: '41000'},
                      {model: 'Nissan Dilaton Flat Iron Revolution',color:'Grey',vin: '4NUES16S746844946', price: '18000'},
                      {model: 'Mercedes-Benz Plasmons Tempura Z',color:'Silver',vin: '2G1FT1EW5B9862653', price: '45000'},
                      {model: 'Sedan Lexus IS 250', color: 'Starfire Pearl', vin:'2B4JB25Z92K361520', price: '30000'},
                      {model: 'Sedan Audi IS 250', color: 'Nebula Gray Pearl', vin:'2G1WB55K779047529', price: '32000'},
                      {model: 'Sedan Lexus IS 250', color: 'Silver Lining Metallic', vin:'1HGCG6574YA335495', price: '36000'},
                      {model: 'Cadillac Zino Julienned Python',color:'White',vin: '3D7KU26633G578622', price: '43000'},
                      {model: 'Lexus Sleptons Barbecued SPITBOL',color:'White',vin: '1B4GP25341B582924', price: '37000'}

                  ])




#Quote Data
Quote.delete_all

#Loan Data
Loan.delete_all

